---
title: Set Up Organizations In AWS Using Federation
date: 2018-03-20 21:49:26
tags:
- aws
subtitle: 'How to setup SSO to your AWS accounts using federation'
cover_index: /assets/covers/aws-organizations.png
cover_detail: rules-cover.jpg
---

# How we setup in accounts in the past
For one assigment couple of months ago I set up a account for each development team.

# Why organizations

# SSO and possibilities

# Open accounts
- open aws account (configure admin role, setup mfa for root)
    - buildeleven.nl
    - mjvdende.com
    - etc
- open auth0 account


<script src="https://embed.runkit.com" data-element-id="my-element"></script>

<!-- anywhere else on your page -->
<div id="my-element">```//Example use meta_data as returned by Auth0
var user = {
    user_metadata: {
        aws_roles: 
        ["ec2", "s3"]
    }
}

// Mapping roles to aws role
var awsRoles = {
      "s3" : "arn:aws:iam::99999999:role/S3RoleP,arn:aws:iam::99999999:saml-provider/auth0SamlProvider",
      "ec2": "arn:aws:iam::55555555:role/Ec2Role,arn:aws:iam::55555555:saml-provider/auth0SamlProvider"

}

// Get roles as defined in user meta_data
userData = user.user_metadata.aws_roles;

// Create the mapping
function mapRoles(roles){
    return awsRoles[roles];
}

// It Works!
console.log(userData.map(mapRoles));```</div>

# Setup Auth0
configure client

# Setup federation in aws

# configure Auth0

# Benefits

# Todo, next post
Automate with terraform
- setup admin roles (mfa, roles, policies)
- 



