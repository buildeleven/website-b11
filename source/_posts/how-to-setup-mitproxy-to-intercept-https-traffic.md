---
title: How to setup mitproxy to intercept HTTPS traffic
date: 2019-05-29 13:14:11
tags:
  - testing
  - web-apps
  - tools
subtitle: 'Useful tips and tricks for mitmproxy'
cover_index: /assets/covers/proxy.jpg
cover_detail: proxy.jpg
---

Capturing traffic from web apps or mobile apps is really easy. You can use a proxy for this. Maybe you already know Charles proxy. I prefer commandline tool mitmproxy. Its free.

This small **how-to** shows you how to setup a proxy.


# Install mitmproxy

You should have Python installed with pip.

``` bash bash
$ python -V
Python 3.7.3
```

If this checks out install mitmproxy with pip: ```pip install mitmproxy --user```

# Using mitmproxy

First write down your ip number. If you are on NATted network use: ```ip addr show``` to find your ip for your network adapter you are using.

Now open terminal and type: ```mitmproxy```. This will start the proxy on port 8080 for you and will generate a unique certificate you can use later on.

# Configure client on mobile phone

First configure your wifi connection to use a proxy. Use the ip number and port from the previous steps.

Now install a **certificate** on your phone by going to mitm.it in browser of your phone. If you do this you can inspect https traffic. More info here: https://docs.mitmproxy.org/stable/concepts-certificates/

Remember for **iPhone** enable the newly installed certificate in **certificate trust settings**.

<img src="ios-certificates.png" alt="ios certificates" style="">

# Common example usages mitmproxy

### Basic

Start ```mitmproxy``` and lookup all commands mitmproxy type ```?```.

If you want to follow the **last request made**: ```shift + f``` command

**Dump only** posts request: ```mitmdump -w dump.mitm '~m POST'``` command

### Filtering

**Capture all requests** to a certain website: ```mitmproxy --view-filter www\.buildeleven\.nl``` command. You can also filter requests with mitmproxy already running. Simply type ```f``` and enter the url you want to filter on.

