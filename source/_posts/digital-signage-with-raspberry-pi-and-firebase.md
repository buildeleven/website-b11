---
title: Digital signage with Raspberry Pi and Firebase
date: 2019-06-18 15:00:35
tags:
  - firebase
  - web-apps
  - cloud
  - raspberry-pi
  - diy
subtitle: 'Simple how to'
cover_index: /assets/covers/signage.jpg
cover_detail: signage-cover.jpg
---

So this post is actually a dump from my notes I had taken during development of a digital signage solution for a client. The client wanted a easy way to update a remote screen.

# Solution 

So I needed to instantly update a remote screen that is connected to a Raspberry Pi. All I needed is a pre-configured Raspberry Pi running a fullscreen chrome browser with a website open that is hosted with Firebase. Firebase hosting includes a realtime database which I used to realtime update the website.

# Firebase hosting

So to set this up you will need a [firebase account](https://firebase.google.com/) and create a new project ex. **signage** within Firebase. The easiest way to get started with Firebase services and serving a simple html page is the use [Firebase hosting](https://firebase.google.com/docs/hosting).

Before we begin setting up a firebase hosting project let's create some realtime data in Firebase. We can use this later on in the examples. Goto the Firebase console and open the **database** view in the develop section and create a database. When Firebase is finished creating the database for you, allow read access to the database:  
``` json
".read": true
```
and insert some key value pairs into the realtime database:

<img src="realtime-data.png" alt="realtime database on firebase" style="width: 100%;">

Now we are ready to create a html file served by Firebase hosting. Easiest way to setup a working example is to use the [Firebase cli](https://firebase.google.com/docs/cli/). With the Firebase cli we set up a new project for **database** and **hosting** like so:

``` bash
$ firebase init                 

     ######## #### ########  ######## ########     ###     ######  ########
     ##        ##  ##     ## ##       ##     ##  ##   ##  ##       ##
     ######    ##  ########  ######   ########  #########  ######  ######
     ##        ##  ##    ##  ##       ##     ## ##     ##       ## ##
     ##       #### ##     ## ######## ########  ##     ##  ######  ########

You're about to initialize a Firebase project in this directory:

  /home/maarten/signage

? Which Firebase CLI features do you want to set up for this folder?
 Press Space to select features, then Enter to confirm your choices.
 ◉ Database: Deploy Firebase Realtime Database Rules
 ◯ Firestore: Deploy rules and create indexes for Firestore
 ◯ Functions: Configure and deploy Cloud Functions
❯◉ Hosting: Configure and deploy Firebase Hosting sites
```

Next choose your project and keep the defaults. We should have ended up with a public directory with an index and 404 file. Change the index.html accordingly:

``` html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Firebase Hosting</title>
    <script defer src="/__/firebase/6.2.0/firebase-app.js"></script>
    <script defer src="/__/firebase/6.2.0/firebase-database.js"></script>
    <script defer src="/__/firebase/init.js"></script>
  </head>
  <body>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        firebase.database().ref('firstname').on('value', snapshot => {
          var firstname = snapshot.val();
          document.getElementById("message").textContent = snapshot.val();
         });
      });
    </script>
    <div id="message">
      <span>wait for data ...</span>
    </div>
  </body>
</html>
```

and test to see if its working

``` bash
$ firebase serve
```

So now we know it's working we can deploy the website. We are only deploying with option **hosting** because we do not want to deploy the database rules.

``` bash
$ firebase deploy --only hosting
```

# Configure Raspberry Pi

Preferred Raspberry Pi disk image: raspian lite and config:

``` bash
$ sudo raspi-config
```

- change user password
- Boot options > Select “Desktop / CLI” and then “Console Autologin”.
- Interfacing options > Enable SSH 
- Network options > change hostname

Install needed packages:

``` bash
$ sudo apt-get install --no-install-recommends \
xserver-xorg x11-xserver-utils xinit openbox chromium-browser
```

Create config file to automatically start Chromeium on signage website
edit: /etc/xdg/openbox/autostart

``` bash
# Disable any form of screen saver / screen blanking / power management
xset s off
xset s noblank
xset -dpms
#
# Allow quitting the X server with CTRL-ATL-Backspace
setxkbmap -option terminate:ctrl_alt_bksp
#
# Start Chromium in kiosk mode
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
chromium-browser --disable-infobars --kiosk 'https://signage.firebaseapp.com'
```

Create script on boot
edit signage.sh:

``` bash
#!/bin/bash
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor
```

then edit .bashrc:

``` bash
source signage.sh
```

Setup wifi:

``` bash
$ wpa_passphrase ssid password | \
sudo tee -a /etc/wpa_supplicant/ wpa_supplicant.conf
```

Open terminal when running:

``` bash
Ctrl-Alt-Backspace
```

