---
title: About
date: 2017-07-18 14:02:43
comments: false
---

Maarten is an independent consult who specializes in devops, cloud and test automation. He lives and works in the Netherlands. Maarten has experience with AWS Cloud, certified AWS Solution architect - associate. 

__Latest Projects__:
- Organized seminar “Continuous Delivery” together with a client
- Hosted meetup events: “Introduction into Docker”, “Webdriver Patterns”, “Mobile testing” 
- Delivered training “Continuous delivery with Docker” at Testworks conf 2016
- Presented at Testworks conf 2015: “Scalable QA with Docker”
- Gave a workshop at Agile testing days 2015: “Scalable QA with Docker”

You should follow him on __twitter__, __linkedin__ and __[blog](/atom.xml)__


